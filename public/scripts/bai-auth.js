/*
/ Authentication - Binding Aitch Industries
/
/ 2010808 - Initial version (Firebase) 
/
*/

var auth;
var database;
var userProfile;
var userEmail;

// Initialiser. This gets called once firebase SDK has finished it's loading. 
function authInit() {
    // Quick links to the firebase stuff
    this.auth = firebase.auth();
    this.database = firebase.database();
	
	// Set up listeners
	auth.onAuthStateChanged(this.onAuthStateChanged); // Successful login or sign out
}

// PUBLIC FUNCTIONS

function emailLogin() {
	// Form for email sign up
    var content = "<div id=\"loginWithEmail\"><p>Login / Signup</p><br>";
    content += "<p><input id=\"loginEmail\" type=\"text\" name=\"email\" placeholder=\"email@example.com\"></p>";
    content += "<p><input id=\"loginPW\" type=\"password\" name=\"password\" placeholder=\"password\"></p>";
	content += '<p id="loginError"></p>';
    content += "<button id=\"submitLogin\" onclick=\"submitLogin()\">Log In</button><br>";
	
	// Fill in the modal
    document.getElementById('usableModalContent').innerHTML = content;
	
	// Pop it
	$('#usableModal').modal({backdrop: 'static', keyboard: false});
}



// LISTENERS

// Account state change
function onAuthStateChanged(user) {
	
    // If user is defined then its a sign in
    if (user) {
		
		database.ref('/users/' + user.uid).once('value').then(function(snapshot) {
			userProfile = snapshot.val();
			if (!snapshot.hasChildren()) {
				userProfile = {};
			}
			userProfile.userKey = user.uid;
			userProfile.toggler = !userProfile.toggler;
			database.ref('users/' + user.uid).update(userProfile);
			database.ref('users/' + user.uid + '/toggler').on('value', function(data) {
				if (data.val() != userProfile.toggler) {
					firebase.auth().signOut(); 
				}
			});
		});
		
		displayAfterLogin();

    } else { //  No user defined means its a sign out


		displayAfterLogout();
	}
};


// PRIVATE FUNCTIONS
function submitLogin() {
	
	userEmail = document.getElementById('loginEmail').value;
	var password = document.getElementById('loginPW').value;
	
	auth.signInWithEmailAndPassword(userEmail, password).then(function(user) {
		// Trigger modal close
		document.getElementById('closeUsableModal').click();
	}).catch(function(error) {
		// Handle Errors here.
		var errorCode = error.code;
		var errorMessage = error.message;
		if (errorCode.endsWith('user-not-found')) {
			document.getElementById('loginError').innerHTML = 'No user exists for that email address. Would you like to Sign Up?';
			document.getElementById('submitLogin').innerHTML = 'Sign Up';
			document.getElementById('submitLogin').setAttribute('onClick', "submitSignup()");
			
		} else {
			console.log(error.code +"2: "+ error.message);
		}
	});
	
	
};

function submitSignup() {
	
	var email = document.getElementById('loginEmail').value;
	var password = document.getElementById('loginPW').value;
	
	// Sign up to firebase
	firebase.auth().createUserWithEmailAndPassword(email, password).catch(function(error) {
  
		// Handle Errors here.
		var errorCode = error.code;
		var errorMessage = error.message;
		console.log(error.code +"3: "+ error.message);
	});
	
	document.getElementById('usableModalContent').innerHTML = '<h2>Sign up successful!</h2><p>You will now be guided through the set up process.</p><button class="btn btn-primary" onClick="profileSetup()">OK!</button>';
};



function profileSetup() {
	document.getElementById('usableModalContent').innerHTML = "";
	var profileFields = ["agentName", "agentPhone", "agentEmail", "agencyName"];
	var fieldLabels = ["Agent Name", "Agent Phone", "Agent Email", "Agency Name"];
	var fieldDiv = document.createElement('div');
	fieldDiv.innerHTML += '<h1>Basic Profile Setup</h1>';
	
	profileFields.forEach(function(fieldName, index) {
		var myRep = '<div class="form-group"><label for="' + fieldName + '">' + fieldLabels[index] + '</label><input type="text" class="form-control" id="' + fieldName + '"></div>';
		fieldDiv.innerHTML += myRep;
	});
	document.getElementById('usableModalContent').appendChild(fieldDiv);
	
	document.getElementById('usableModalContent').innerHTML += '<div class="row btn-toolbar"><button type="button" class="btn btn-primary" onClick="saveProfile()">Save</button></div>';
	
	document.getElementById('agentEmail').value = userEmail;
	
	// A successful signup will have logged in and closed the modal.  Show it again.
	$('#usableModal').modal({backdrop: 'static', keyboard: false});
}

function saveProfile() {
	var profileFields = ["agentName", "agentPhone", "agentEmail", "agencyName"];
	var post = {};
	
	// Grab the input fields from the modal
	profileFields.forEach(function(fieldName) {
		post[fieldName] = document.getElementById(fieldName).value;
	});
	
	// Push it up to firebase
	database.ref('users/' + userProfile.userKey).update(post);
	
	// First time use instructions
	var instructions = '<h1>Before you start...</h1><p>This application requires additional setup. A major function of Amicus is the ability to populate and manage various documents related to the sale of a property. These documents vary from agent to agent and therefore require custom set up by an administrator. Please contact <a href="mailto:ba-industries@gmail.com">Binding Aitch Industries</a> to arrange conversion and upload of your agreements. contracts and other documents.</p><p>In the meantime, you can customise your own email templates for sourcing Photoraphy and Building services in the Templates section of the menu.</p>';
	instructions += '<div class="row btn-toolbar" style="padding-top:25px;"><button type="button" class="btn btn-primary" onClick="document.getElementById(\'closeUsableModal\').click();">Close</button></div>';
	
	document.getElementById('usableModalContent').innerHTML = instructions;
}

function getUserProfile() {
		return JSON.stringify(userProfile);
}