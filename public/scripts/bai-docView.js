
var auth;
var database;
var fieldList;
var params;
var transaction;

function docView() {
	// Quick links to the firebase stuff
    this.database = firebase.database();
	
	// Init some vars
	params = new Object();
	var mapping;
	
	// Grab the job details from the URL
	var argString = window.location.search.substring(1);
	var paramArray = argString.split("&");
	for (index = 0; index < paramArray.length; index++) {
	   var keyValue = paramArray[index].split("=");
	   params[keyValue[0]] = keyValue[1];
	}
	
	
	fieldList = [];

	var displayDiv = document.getElementById('page');
	
	database.ref('transactions/' + params.ref_1 + '/' + params.ref_2).once('value').then(function(transSnapshot) {
		transaction = transSnapshot.val();
		
		return database.ref('docs/' + params.ref_1 + '/' + params.doc).once('value');
	}).then(function(snapshot) {
		if (snapshot.val() == null) {
			displayDiv.innerHTML = '<p>An error has occured. Either the link has expired or permissions do no allow you to view this document. Please contact Binding Aitch Industries if you wish to troubleshoot this error.</p>';
		} else {
			displayDiv.innerHTML = decodeURIComponent(snapshot.val().doc);
		}
		snapshot.child('mapping').forEach(function (mapping) {
			if (transaction[mapping.val().field]) {
				if (mapping.val().type == "text") {
					
					// Put the value in the document
				    document.getElementById(mapping.key).innerHTML = transaction[mapping.val().field];
				}
				if (mapping.val().type == "date") {
					// Strip out the time portion and only keep the date
					var givenDate = new Date(transaction[mapping.val().field]).toDateString();
					
					// Put the value in the document
					document.getElementById(mapping.key).innerHTML = givenDate;
				}
				if (mapping.val().type == "dropdown") {
					
					// Put the value in the document
					mapping.val().options.forEach(function(option) {
						if (transaction[mapping.val().field] == option.label) {
							document.getElementById(option.ref).setAttribute('style', "display: inline");
						} else {
							document.getElementById(option.ref).setAttribute('style', "display: none");
						}
					});
				}
			}
		});
		
		window.print();
	});
		
}


function docPrint() {
	var printable = window.open();
	printable.document.write('<html><head>' + document.head.innerHTML + '</head><body>' + document.getElementById('displayPane').innerHTML + '</body></html>');
	setTimeout(function() {
		printable.print();
		printable.close();		
	}, 2000);
	
	
	// TODO store a flag to say the pdf has been printed. We can use this as a warning when someone attempts to edit it later.

}