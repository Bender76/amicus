var activeTransaction;
var templates;
var params;
var providerCat;
var transRef;
var jobParticulars;

function goHome() {
		
}


function displayAfterLogout() {
	// goHome();
	displayAfterLogin();
}

function displayAfterLogin() {
	// Grab the event keys from the URL
	var argString = window.location.search.substring(1);
	params = {};
	jobParticulars = {};
	var paramArray = argString.split("&");
	for (index = 0; index < paramArray.length; index++) {
	   var keyValue = paramArray[index].split("=");
	   params[keyValue[0]] = keyValue[1];
	}
	
	// Display file upload layout
	var pageContent = '<div class="row col-md-12" style="text-align: center; padding-bottom: 35px"><h1 id="title">Upload Facility</h1></div><div class="row"><div class="col-md-2">&nbsp;</div><div class="col-md-8"><div class="panel panel-default"><div class="panel-heading">Files</div><div class="panel-body" id="panelContent"><span id="completed"></span><span id="processing"></span><div class="gallery"><img src="images/addFile.png" alt="Add Files" style="cursor: pointer; width: 100px; height: auto;" onclick="fileSelector()"><div class="desc"><p>Upload files...</p></div></div></div></div></div><div class="col-md-2">&nbsp;</div></div>';
	document.getElementById('page').innerHTML = pageContent;
	
	var fileList = '';
	
	// Display list of files already uploaded
	database.ref('uploads/' + params['key'] + '/' + params['bucket'] + '/' + params['provider']).on('child_added', function(snapshot) {
		var newFileThumb = document.createElement('div');
		newFileThumb.setAttribute('id', 'thumb_' + snapshot.val().filename);
		newFileThumb.setAttribute('class', 'gallery');
		newFileThumb.innerHTML = '<img src="images/file.png" alt="File" style="cursor: pointer; width: 100px; height: auto;" onclick="fileSelector()" id="img_' + snapshot.val().filename + '"><div class="desc">' + snapshot.val().filename + '</div>';
		document.getElementById('completed').appendChild(newFileThumb);
		if (snapshot.val().filename.endsWith('jpg') || snapshot.val().filename.endsWith('jpeg') || snapshot.val().filename.endsWith('png')) {
			firebase.storage().ref().child(params['key'] + '/' + params['bucket'] + '/' + params['provider'] + '/' + snapshot.val().filename).getDownloadURL().then(function(url) {
				document.getElementById('img_' + snapshot.val().filename).setAttribute('src', url);
			});
		}
		
	});
}

function fileSelector () {
	var uploadModal = '<h2>File Selector</h2><label class="btn btn-default btn-file">Browse...<input type="file" id="selectedFiles" style="display: none;" onChange="uploadFiles()" multiple></label>';
	
	document.getElementById('usableModalContent').innerHTML = uploadModal;
	$('#usableModal').modal('show');
}

function uploadFiles() {
	// Trigger modal close
	document.getElementById('closeUsableModal').click();
	
	var newAdditions = '';
	var fileList = document.getElementById('selectedFiles').files;
	for (index = 0; index < fileList.length; index++) {
		var newFileThumb = '<div class="gallery" id="thumb_' + fileList[index].name + '"><img src="images/file.png" alt="File" style="cursor: pointer; width: 100px; height: auto;" onclick="fileSelector()"><div class="desc"><div class="progress" style="text-align: center;"><div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%" id="' + fileList[index].name + '"></div></div>' + fileList[index].name + '</div></div>';
		newAdditions += newFileThumb;
	}
	document.getElementById('processing').innerHTML += newAdditions;
	doUpload();
}

// Upload a new template
function doUpload() {
	
	// Get list of files for upload
	fileList = document.getElementById('selectedFiles').files;
	
	// Upload each file
	for (index = 0; index < fileList.length; index++) {
		var uploadRef = firebase.storage().ref(params['key'] + "/" + params['bucket'] + '/' + params['provider'] + '/' + fileList[index].name);
		var filename;
		task = uploadRef.put(fileList[index]);
		task.on('state_changed', function(snapshot) {
			filename = snapshot.ref.name;
			var progress = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
			
			var progBar = document.getElementById(filename);
			progBar.setAttribute('aria-valuenow', progress);
			progBar.setAttribute('style', "width:" + progress + "%");
			
		}, function (error) {
			// Handle the demons
		}, function(snapshot) {
			
			// Remove the thumbnail from the progress element
			document.getElementById('processing').removeChild(document.getElementById('thumb_' + filename));
			
			// Update the db index
			database.ref('uploads/' + params['key'] + '/' + params['bucket'] + '/' + params['provider']).push({filename: filename});
			
		});
	}
}

