
var auth;
var selectedDate;
var clickedEvent;
var providerList;

function schedulerInit() {
	// init some variables
	providerList = {};
	
	// Quick links to the firebase stuff
    this.database = firebase.database();
	
	// Scope my param
	this.docRef = docRef;
	
	// Draw the page
	pageDiv = '<div class="row col-12" style="text-align: center; padding-bottom: 65px;"><h1 id="title">Scheduler</h1></div>';
	pageDiv += '<div id="root"><div class="col-md-9" id="monthCalendar"></div><div class="col-md-3" id="eventDisplay">&nbsp;</div></div>';
	
	document.getElementById('page').innerHTML = pageDiv;
	
	drawScheduler();
}

function drawScheduler() {
	
	// Drop in the calendar
	$('#monthCalendar').fullCalendar({
		
		// Defaults
		defaultView: 'month',
		selectable: true,
		
		// React to a click on an event
		eventClick: function(calEvent, jsEvent, view) {
			
			database.ref('events/' + userProfile.userKey + '/' + calEvent.id).once('value').then(function(snapshot) {
			
				// Titles are important
				var eventContent = '<h3>' + calEvent.start.format("DD MMM YYYY") + '</h3>';
				
				// I've got the time tick tick ticking in my head - Joe Jackson
				clickedEvent = snapshot.val();
				var sTime = new Date(snapshot.val().startTime);
				var sDisplay = sTime.getHours() + ':' + zeroPadTime(sTime.getMinutes());
				var eTime = new Date(snapshot.val().endTime);
				var eDisplay = eTime.getHours() + ':' + zeroPadTime(eTime.getMinutes());
				eventContent += '<p><input type="time" id="selStart" value="' + sDisplay + '" onChange="updateEvent()"> - <input type="time" id="selEnd" value="' + eDisplay + '" onChange="updateEvent()"></p>';
				// Details go here
				eventContent += '<p>Description: <input type="text" id="selDesc" value="' + snapshot.val().eventDescription + '" onChange="updateEvent()"></p>';
				eventContent += '<p>Location: ' + calEvent.title + '</p>';
				eventContent += '<p>Services:</p>';		
				eventContent += '<div id="attendeeList"></div>';
				eventContent += '<p style="cursor: pointer;" onClick="addService()">+ Add participant...</p>';
				document.getElementById('eventDisplay').innerHTML = eventContent;
				
				// **** CHANGE THIS TO A DB LISTENER WITH CHILD_ADDED!!!!!
				snapshot.child('attendees').forEach(function(attendee) {
					database.ref('providers/' + userProfile.userKey + '/' + attendee.val().ref).once('value').then(function(snapshot) {
						document.getElementById('attendeeList').innerHTML += '<p>' + snapshot.val().providerName + '</p>';
					});
				});
			});
		},
		
		// React to clicking on a non-event
		select: function(start, end, event, view) {
			
			// Get the date
			var selectedDate = new Date(start);
			
			// Set up the modal
			$('#modalTitle').text(selectedDate.toDateString());
			var content = '<div class="row"><div class="col-md-2" style="padding-right: 0px;">Start Time:</div><div class="col-md-4"><input type="time" id="startTime"></div>';
			content += '<div class="col-md-2" style="padding-right: 0px;">End Time:</div><div class="col-md-4"><input type="time" id="endTime"></div></div>';
			content += '<div class="row"><div class="col-md-2" style="padding-right: 0px;">Description:</div><div class="col-md-10"><input type="text" id="eventDescription"></div></div>';
			content += '<div class="row"><div class="col-md-2" style="padding-right: 0px;">Location:</div><div class="col-md-10"><select id="eventLocation"><select></div></div>';
			content += '<div class="row btn-toolbar"><button type="button" class="btn btn-primary" data-time="' + selectedDate.toString() + '" onClick="saveEvent(this.dataset.time)">Save</button>';
			content += '<button type="button" class="btn btn-primary" onClick="document.getElementById(\'closeUsableModal\').click()">Cancel</button></div>';
			document.getElementById('usableModalContent').innerHTML = content;
			
			// Query firebase to get a list of active transactions to display in the location dropdown
			database.ref('transactions/' + userProfile.userKey).once('value').then(function(snapshot) {
				snapshot.forEach(function(childSnapshot) {
					
					// Only adding good ones
					if (childSnapshot.val().propertyAddress) {
						var newOpt = '<option value="' + childSnapshot.key + '">' + childSnapshot.val().propertyAddress + '</option>';
						document.getElementById('eventLocation').innerHTML += newOpt;
					}
				});
			});
			
			// Modal me baby
			$('#usableModal').modal('show');
		}
	}); 
	
	// Load in data 
	database.ref('events/' + userProfile.userKey).on('child_added', function (event) {	
		var loadedEvent = event.val();
		loadedEvent.id = event.key;
		
		// Grab the transaction for this event so we can get the address
		database.ref('transactions/' + userProfile.userKey + '/' + loadedEvent.transactionRef).once('value').then(function(snapshot) {
		
			// Create an obj that fullcalendar can render
			var calendarEvent = {};
			calendarEvent['start'] = moment(loadedEvent.startTime);
			calendarEvent['end'] = moment(loadedEvent.endTime);
			calendarEvent['title'] = snapshot.val().propertyAddress;
			calendarEvent['id'] = loadedEvent.id;
			
			
			// TODO Color code the events based on status
			
			$('#monthCalendar').fullCalendar('renderEvent', calendarEvent, true);
		});
	});

}

function addService() {
	
	// Make roles list
	document.getElementById('usableModalContent').innerHTML = '<p id="catSelector"><select id="providerCat" onClick="populateProviderSelector()"></select></p>';
	
	// Make providers list
	document.getElementById('usableModalContent').innerHTML += '<p><select id="providerSelect" onClick="providerToEmail()"></select></p>';
	
	// Put in a warning field
	document.getElementById('usableModalContent').innerHTML += '<p id="emailWarning" style="visibility: hidden; color: red;">Email requires information that is not yet available.</p>';
	
	// Put in the field for the email
	document.getElementById('usableModalContent').innerHTML += '<p><textarea id="emailContent" rows="10" cols="64"></textarea></p>';
	
	// Put in the buttons
	document.getElementById('usableModalContent').innerHTML += '<div class="row btn-toolbar"><button type="button" class="btn btn-primary" onClick="saveAndSend()">Send and Save</button><button type="button" class="btn btn-primary" onClick="document.getElementById(\'closeUsableModal\').click()">Cancel</button></div>';
	
	// Query the possible providers for each role
	database.ref('providers/' + userProfile.userKey).once('value').then(function(snapshot) {
		providerList = {};
		snapshot.forEach(function(childSnapshot) {
			
			if (!providerList[childSnapshot.val().providerCategory]) {
				providerList[childSnapshot.val().providerCategory] = [];
				
				// Populate the roles drop down
				document.getElementById('providerCat').innerHTML += '<option value="' + childSnapshot.val().providerCategory + '">' + childSnapshot.val().providerCategory + '</option>';
			}
			
			// Populate the provider array
			providerList[childSnapshot.val().providerCategory].push(childSnapshot.val());
		});
		
	});
	
	
	
	// Put up the modal
    $('#usableModal').modal('show');
	
}

function populateProviderSelector() {
	var domRef = document.getElementById('providerCat');
	var catSpecificList = providerList[domRef.options[domRef.selectedIndex].value];
	
	document.getElementById('providerSelect').innerHTML = "";

	Object.keys(catSpecificList).forEach(function(key) {
		document.getElementById('providerSelect').innerHTML += '<option value="' + key + '">' + catSpecificList[key].providerName + '</option>';
	});
	
	populateEmailBox();
}
	
function populateEmailBox(selectedProvider) {
	var domRef = document.getElementById('providerCat');
	
	// Grab the appropriate email template for this category
	database.ref('templates/'+ userProfile.userKey + '/' + domRef.options[domRef.selectedIndex].value).once('value').then(function(template) {
		var emailContent = template.val();
		
		// TODO this may only be needed for the hand baked templates. I suspect when I get the proper editor working it wont be needed
		//emailContent = emailContent.replace(/\\n/g, '\n');
		
		// Sub in agent values
		Object.keys(userProfile).forEach(function(key) {
			var keyRegex = new RegExp(escapeRegExp('{$' + key + '}'), 'g');
			emailContent = emailContent.replace(keyRegex, userProfile[key]);
		});
		
		// Sub in event values
		var startTime = new Date(clickedEvent.startTime);
		var dateRegex = new RegExp(escapeRegExp('{$eventTime}'), 'g');
		emailContent = emailContent.replace(dateRegex, startTime.toLocaleString());
		
		// Sub in the provider name
		if (selectedProvider) {
			var providerRegex = new RegExp(escapeRegExp('{$providerName}'), 'g');
			emailContent = emailContent.replace(providerRegex, selectedProvider.providerContact);
		}
		
		// Sub in job aceptance link
		var acceptLink = "https://amicus-510bb.firebaseapp.com/acceptInvite.html?key=" + userProfile.userKey + "&event=" + clickedEvent.ref + "&attendee=" + selectedProvider.ref;
		var linkRegex = new RegExp(escapeRegExp('{$eventLink}'), 'g');
		emailContent = emailContent.replace(linkRegex, acceptLink);
		console.log(acceptLink);
		
		database.ref('transactions/' + userProfile.userKey + '/' + clickedEvent.transactionRef).once('value').then(function(snapshot) {
		
			// Sub in transaction values
			snapshot.forEach(function(childSnapshot) {
				var keyRegex = new RegExp(escapeRegExp('{$' + childSnapshot.key + '}'), 'g');
				emailContent = emailContent.replace(keyRegex, childSnapshot.val());
			});
			
			document.getElementById('emailContent').value = emailContent;
			
			// Test if there are any remaining substitutions and warn user
			var subRegex = new RegExp('\\{\\$[^\\}]+\\}');
			if (subRegex.test(emailContent)) {
				document.getElementById('emailWarning').style.visibility = 'visible';
			} else {
				document.getElementById('emailWarning').style.visibility = 'hidden';
			}
		});
	});
}

function providerToEmail() {
	var provider = getSelectedProvider();
	
	populateEmailBox(provider);
	
}

function saveAndSend() {
	// Grab the email content from the modal
	var emailContent = document.getElementById('emailContent').value ;
	
	// Grab the selected provider
	var provider = getSelectedProvider();
	
	// Queue mail
	var mailPost = {};
	mailPost['personalizations'] = [];
	var toArray = [];
	toArray.push({email: provider.providerEmail});
	mailPost['personalizations'].push({to: toArray, subject: 'Job Request'});
	mailPost['from'] = {email: userProfile.agentEmail};
	var mailContent = {};
	mailContent['type'] = 'text/plain';
	mailContent['value'] = emailContent;
	mailPost['content'] = [mailContent];

	var jsonEmail = JSON.stringify(mailPost);
	
	database.ref('emailQueue/').push({email: jsonEmail});
	
    /*database.ref('emailQueue/').once('child_added', function(snapshot) {
		snapshot.forEach(function(childSnapshot) {
			console.log(JSON.parse(childSnapshot.val().email));
		});
	});*/
	
	// Add the provider as a pending attendee
	var post = {};
	console.log(provider);
	post['ref'] = provider.ref;
	post['status'] = 'pending';
	database.ref('events/' + userProfile.userKey + '/' + clickedEvent.ref + '/attendees/' + provider.ref).update(post);
	
	// Set status in the transaction
	var field = document.getElementById('providerCat').value + ' Status';
	field = camelize(field);
	//console.log('transactions/' + 
	
	// Close the modal
	document.getElementById('closeUsableModal').click();
}

function getSelectedProvider() {
	var domRef = document.getElementById('providerCat');
	var catSpecificList	= providerList[domRef.options[domRef.selectedIndex].value];
	domRef = document.getElementById('providerSelect');
	return catSpecificList[domRef.options[domRef.selectedIndex].value];
}

function zeroPadTime(digits) {
	if (digits < 10) {
		return '0' + digits;
	} else {
		return digits;
	}
}

function escapeRegExp(s) {
    return s.replace(/[-/\\^$*+?.()|[\]{}]/g, '\\$&')
}

function saveEvent(selectedDate) {
	// Grab the data into an object for saving
	var post = {};
	var sTime = document.getElementById('startTime').value;
	var sHr = sTime.slice(0, sTime.indexOf(':'));
	var sMin = sTime.slice(sTime.indexOf(':')+1);
	var eTime = document.getElementById('endTime').value;
	var eHr = eTime.slice(0, eTime.indexOf(':'));
	var eMin = eTime.slice(eTime.indexOf(':')+1);
	var myDate = new Date(selectedDate);
	myDate.setHours(sHr);
	myDate.setMinutes(sMin);
	post['startTime'] = myDate.valueOf();
	myDate.setHours(eHr);
	myDate.setMinutes(eMin);
	post['endTime'] = myDate.valueOf();
	post['eventDescription'] = document.getElementById('eventDescription').value;
	var locRef = document.getElementById('eventLocation');
	post['transactionRef'] = locRef.options[locRef.selectedIndex].value;
	post['ref'] = database.ref('events/' + userProfile.userKey).push().key;
	
	// Save to db
	database.ref('events/' + userProfile.userKey + '/' + post.ref).update(post);
	
	// Close the modal
	document.getElementById('closeUsableModal').click();
}

function updateEvent() {
	console.log(clickedEvent);
}
