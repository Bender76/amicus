var activeTransaction;
var templates;
var params;
var providerCat;
var transRef;
var jobParticulars;

function goHome() {
		
}


function displayAfterLogout() {
	// goHome();
	displayAfterLogin();
}

function displayAfterLogin() {
	// Grab the event keys from the URL
	var argString = window.location.search.substring(1);
	params = {};
	jobParticulars = {};
	var paramArray = argString.split("&");
	for (index = 0; index < paramArray.length; index++) {
	   var keyValue = paramArray[index].split("=");
	   params[keyValue[0]] = keyValue[1];
	}
	
	// Display job acceptance options
	var pageContent = '<div class="row col-md-12" style="text-align: center; padding-bottom: 35px"><h1 id="title">Job Acceptance</h1></div><div class="row"><div class="col-md-3">&nbsp;</div><div class="col-md-6"><span id="jobDetails"></span></div><div class="col-md-3">&nbsp;</div></div>';
	document.getElementById('page').innerHTML = pageContent;
	
	// What's my category?
	database.ref('providers/' + params['key'] + '/' + params['attendee']).once('value').then(function(snapshot) {
		providerCat = snapshot.val().providerCategory;
		return database.ref('events/' + params['key'] + '/' + params['event']).once('value');
	}).then(function(snapshot) { // Check if the job has already been taken
	    var catAcceptKey = providerCat + "Accepted";
		if (snapshot.child(catAcceptKey).exists()) {
			
			// Job taken....sorry
			document.getElementById('jobDetails').innerHTML = 'This job has already been accepted by another provider.';			
			
		} else {
			
			// Grab the job details from the db
			var details = '<p>Do you want to accept the following job?</p>';
			database.ref('events/' + params['key'] + '/' + params['event']).once('value').then(function(snapshot) {
				transRef = snapshot.val().transactionRef;
				var startDate = new Date(snapshot.val().startTime);
				var endDate = new Date(snapshot.val().endTime);
				details += getDayName(startDate.getDay()) + ' ' + startDate.getDate() + ' ' + getMonthShortName(startDate.getMonth()) + '<br>';
				details += get12HrTime(startDate.getHours(), startDate.getMinutes()) + ' - ' + get12HrTime(endDate.getHours(), endDate.getMinutes()) + '<br>';
				jobParticulars['startDate'] = snapshot.val().startTime;
				jobParticulars['endDate'] = snapshot.val().startTime;
				return database.ref('transactions/' + params['key'] + '/' + snapshot.val().transactionRef).once('value');
			}).then(function(snapshot) {
				details += snapshot.val().propertyAddress + ', ' + snapshot.val().division + '<br><br>';
				details += '<button onClick="acceptJob()">Accept</button><button onClick="rejectJob()">No thanks</button>';
				jobParticulars['propertyAddress'] = snapshot.val().propertyAddress;
				jobParticulars['clientName'] = snapshot.val().clientName;
				document.getElementById('jobDetails').innerHTML = details;
			});
			
		}
	});
	
	
	
}

function acceptJob() {
	// Set my entry to acccepted
	if (providerCat) {
		catAcceptKey = providerCat + 'Accepted';
		var post = {};
		post[catAcceptKey] = params['attendee'];
		database.ref('events/' + params['key'] + '/' + params['event']).update(post);
		console.log('done');
	}
	
	// Get the providers email address
	var destEmail;
	var poc;
	database.ref('providers/' + params['key'] + '/' + params['attendee']).once('value').then(function(snapshot) {
		destEmail = snapshot.val().providerEmail;
		poc = snapshot.val().providerContact;
		return database.ref('users/' + params['key']).once('value');
	}).then(function(snapshot) {
		
		// Generate upload link
		var uploadLink = 'http://amicus-510bb.firebaseapp.com/fileUpload.html?key=' + params['key'] + '&bucket=' + transRef + '&provider=' + params['attendee'];
		
		// Generate email
		var emailBody = 'Hi ' + poc +',\n\n--Do Not Reply To This Email--\n\nThanks for accepting the job. Attached is a calendar entry which you can import to your personal calendar.\n\nPlease use this link to submit your work.\n' + uploadLink + '\n\nIf you would like to contact ' + snapshot.val().agentName + ' please use ';
		var reg = new RegExp('^\\d+$');
		if (reg.test(snapshot.val().agentPhone)) {
			emailBody += snapshot.val().agentPhone + ' or ';
		}
		emailBody += snapshot.val().agentEmail + '.\n\nThanks\nAmicus Software\n';
		
		// Create ics attachment
		var ics = 'BEGIN:VCALENDAR\nVERSION:2.0\nPRODID:-//Amicus//NONSGML 1.0//EN\nBEGIN:VEVENT\n';
		ics += 'SUMMARY:Job - ' + jobParticulars['clientName'] + '\n';
		ics += 'UID:' + database.ref('/').push().key + '@amicus.com.au\n';
	    ics += 'DTSTART:' + getICSDateTime(jobParticulars['startDate']) + '\n';
		ics += 'DTEND:' + getICSDateTime(jobParticulars['endDate']) + '\n';
		ics += 'DTSTAMP:' + getICSDateTime(Date.now()) + '\n';
		ics += 'DESCRIPTION: After completing the job please visit ' + uploadLink + ' to upload your submission.\n';
		ics += 'LOCATION:' + jobParticulars['propertyAddress'] + '\n';
		ics += 'END:VEVENT\nEND:VCALENDAR';
		
		// Send email with upload link
		var mailPost = {};
		mailPost['personalizations'] = [];
		var toArray = [];
		toArray.push({email: destEmail});
		mailPost['personalizations'].push({to: toArray, subject: 'Amicus Job Details and Upload Link'});
		mailPost['from'] = {email: 'do-not-reply@amicus.com.au'};
		var mailContent = {};
		mailContent['type'] = 'text/plain';
		mailContent['value'] = emailBody;
		mailPost['content'] = [mailContent];
		var attachment = {};
		attachment['filename'] = 'calendarEntry.ics';
		attachment['content'] = btoa(ics);
		mailPost['attachments'] = [];
		mailPost['attachments'].push(attachment);
		var jsonEmail = JSON.stringify(mailPost);
		console.log(jsonEmail);
		database.ref('emailQueue/').push({email: jsonEmail});
		
		// Display what happens next
		document.getElementById('jobDetails').innerHTML = 'Thanks for accepting the job. An email with details of the job have been sent to ' + destEmail + '. The email also contains a link to an upload page. Please use this link to submit your work.';
	});
	
}

function rejectJob() {
	// Remove attendee from the list
	database.ref('events/' + params['key'] + '/' + params['event'] + '/attendees/' + params['attendee']).remove();
}

// Stolen from stack overflow
function camelize(str) {
  return str.replace(/(?:^\w|[A-Z]|\b\w)/g, function(letter, index) {
    return index == 0 ? letter.toLowerCase() : letter.toUpperCase();
  }).replace(/\s+/g, '');
}