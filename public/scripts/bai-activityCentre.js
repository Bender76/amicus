

function activityCentreInit() {

	// Quick links to the firebase stuff
    this.database = firebase.database();
	
	// Draw the page
	pageDiv = '<div class="row col-12" style="text-align: center; padding-bottom: 65px;"><h1 id="title">Activity Centre</h1></div>';
	pageDiv += '<div class="row col-md-12"><table class="table table-hover" id="activityTable"><thead><tr><th>Property</th><th>Agency Agreement</th><th>Request For Contract</th><th>Photographer</th><th>Builder</th></tr></thead><tbody id="tableBody"></tbody></table></div>';
	
	document.getElementById('page').innerHTML = pageDiv;
	
	populateBody();
}

function populateBody () {
	document.getElementById('tableBody').innerHTML = "";
	database.ref('transactions/' + userProfile.userKey).once('value').then(function (snapshot) {
		snapshot.forEach(function (childSnapshot) {
			var fullAddy = '';
			if (childSnapshot.val().propertyAddress) {
				fullAddy = childSnapshot.val().propertyAddress;
			} else {
				fullAddy = 'Unspecified Address';
			}
			if (childSnapshot.val().division) {
				fullAddy += ', ' + childSnapshot.val().division;
			} 
			var tRow = '<tr><td style="width: 40%">' + fullAddy + '</td>';
			if (childSnapshot.val().agencyAgreementCompleted)  {
				tRow += '<td><span onClick="docLauncher(\'Agency Agreement\', \''+ childSnapshot.key + '\')" style="cursor: pointer;">Complete</span></td>';
			} else {
				tRow += '<td><span onClick="docLauncher(\'Agency Agreement\', \''+ childSnapshot.key + '\')" style="cursor: pointer;">Incomplete</span>&nbsp;<span onClick="docLauncher(\'Agency Agreement\', \''+ childSnapshot.key + '\')" style="cursor: pointer;" class="glyphicon glyphicon-edit"></span></td>';
			}
			if (childSnapshot.val().requestForContractCompleted)  {
				tRow += '<td><span onClick="docLauncher(\'Request For Contract\', \''+ childSnapshot.key + '\')" style="cursor: pointer;">Complete</span></td>';
			} else {
				tRow += '<td><span onClick="docLauncher(\'Request For Contract\', \''+ childSnapshot.key + '\')" style="cursor: pointer;">Incomplete</span>&nbsp;<span onClick="docLauncher(\'Request For Contract\', \''+ childSnapshot.key + '\')" style="cursor: pointer;" class="glyphicon glyphicon-edit"></span></td>';
			}
			if (childSnapshot.val().photographerStatus)  {
				tRow += '<td>'+ childSnapshot.val().photographerStatus + '</td>';
			} else {
				tRow += '<td><span onClick="schedulerInit()" style="cursor: pointer;">Pending</span>&nbsp;<span onClick="schedulerInit()" style="cursor: pointer;" class="glyphicon glyphicon-calendar"></span></td>';
			}
			if (childSnapshot.val().builderStatus)  {
				tRow += '<td>'+ childSnapshot.val().builderStatus + '</td>';
			} else {
				tRow += '<td><span onClick="schedulerInit()" style="cursor: pointer;">Pending</span>&nbsp;<span onClick="schedulerInit()" style="cursor: pointer;" class="glyphicon glyphicon-calendar"></span></td>';
			}
			tRow += '</tr>';
			document.getElementById('tableBody').innerHTML += tRow;
		});
	});
}

function docLauncher (title, transaction) {
	database.ref('transactions/' + userProfile.userKey + '/' + transaction).once('value').then(function(snapshot) {
		docInputInit(camelize(title), title, snapshot.val());
	});
}