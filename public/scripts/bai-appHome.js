var activeTransaction;
var templates;

function goHome() {
		document.getElementById('page').innerHTML = '<div class="row" style="padding-top: 16%; text-align: center"><img class="dashIconShaded" src="images/sellProperty.jpg" id="agencyAgreement" data-title="Agency Agreement" data-target="docInputInit"><img class="dashIconShaded" src="images/activityCentre.png" id="activityCentre" data-target="activityCentreInit"><img class="dashIconShaded" src="images/scheduler.png" id="scheduler" data-target="schedulerInit"></div><div class="row" id="displayBar" style="text-align:center; padding-top:2%; "><p style="font-style: italic; color: #999999; cursor: pointer;" onClick="emailLogin()">Login/Signup</p></div>';
		
		// Hide the menu
		document.getElementById('menu').setAttribute('style', 'display: none');
}

function load(clicked) {
	activeTransaction = {};
	activeTransaction['ref'] = database.ref('transactions/' + userProfile.userKey).push().key;
	
	// Call the function specified in data-target with the parameter set in id
	if (clicked.dataset.title) {
		window[clicked.dataset.target](clicked.id, clicked.dataset.title, activeTransaction);
	} else {
		window[clicked.dataset.target]();
	}
}

function manageProfile() {
	var profileForm = '<div class="row col-12" style="text-align: center;"><h1 id="title">Basic Profile Setup</h1></div>';
	profileForm += '<div class="row"><div class="col-md-4">&nbsp</div><div class="col-md-4">';
	var fieldLabels = ["Agent Name", "Agent Phone", "Agent Email", "Agency Name"];
	
	fieldLabels.forEach(function(label) {
		var myRep = '<div class="form-group"><label for="' + camelize(label) + '">' + label + '</label><input type="text" class="form-control" id="' + camelize(label) + '"></div>';
		profileForm += myRep;
	});
	
	profileForm += '<button type="button" class="btn btn-primary" onClick="saveProfile()">Save</button></div><div class="col-md-4">&nbsp</div></div>';
	
	document.getElementById('page').innerHTML = profileForm;
	
	// Populate known values
	database.ref('users/' + userProfile.userKey).once('value').then(function(snapshot) {
		fieldLabels.forEach(function(label) { 
			if (snapshot.val()[camelize(label)]) {
				document.getElementById(camelize(label)).value = snapshot.val()[camelize(label)];
			}
		});
	});
}

function manageTemplates() {
	var pageContent = '<div class="row col-12" style="text-align: center;"><h1 id="title">Manage Email Templates</h1></div>';
	pageContent += '<div class="row"><div class="col-md-2">&nbsp</div><div class="col-md-6">';
	pageContent += '<p>Email template for:</p><p><select id="providerList" onClick="toggleTemplate()"></select></p><textarea id="templateArea" cols=96 rows=12></textarea><div class="row btn-toolbar"><button type="button" class="btn btn-primary"  onClick="saveTemplate()">Save</button><button type="button" class="btn btn-primary" onClick="reset()">Reset</button></div><p style="all: initial" id="message"></p>';
	pageContent += '</div><div class="col-md-3" id="insertables">';
	pageContent += '</div><div class="col-md-1">&nbsp</div></div>';
	
	document.getElementById('page').innerHTML = pageContent;
	
	templates = {};
	$('#message').fadeOut();
	
	// Grab a list of templates and whack it in the drop down
	database.ref('templates/' + userProfile.userKey).once('value').then(function(snapshot) {
		snapshot.forEach(function(providerCat) {
			document.getElementById('providerList').innerHTML += '<option>' + providerCat.key + '</option>';
		});
		
		return database.ref('templates/' + userProfile.userKey).once('value');
	}).then(function(snapshot) {
		snapshot.forEach(function(providerCat) {
			templates[providerCat.key] = providerCat.val();
		});
		// Display the first cat
		document.getElementById('providerList').selectedIndex = 0;
		toggleTemplate();
	});
	
	// Grab a list of fields and associated mappings
	// TODO I'm not sure how to select a list of appropriate buttons. User Profile fields seem obvious but which transaction fields have meaning?  Might need a config 
	// style of table. Until then I'm just coding it
	fieldList = ['Agent Name', 'Agent Phone', 'Agent Email', 'Agency Name', 'Client Name', 'Client Phone', 'Client Email', 'Property Address', 'Provider Name', 'Event Time'];
	
	// Make a button per field
	fieldList.forEach(function(field) {
		var button = '<button type="button" class="btn btn-info" id="' + camelize(field) + '" onClick="insertMe(this.id)">' + field + '</button>';
		document.getElementById('insertables').innerHTML += button;
	});
	
	
}

function insertMe(field) {
  var cursorPos = document.getElementById("templateArea").selectionStart;
  var existingText = document.getElementById("templateArea").value;
  field = '{$' + field + '}';
  var newText = [existingText.slice(0, cursorPos), field, existingText.slice(cursorPos)].join('');
  document.getElementById("templateArea").value = newText;
  cursorPos += newText.length;
  document.getElementById("templateArea").focus();
}

function toggleTemplate() {
	var domRef = document.getElementById('providerList');
	var templateContent = templates[domRef.options[domRef.selectedIndex].value];
	document.getElementById('templateArea').value = templateContent;
}

function saveTemplate() {
	var domRef = document.getElementById('providerList');
	var post = {};
	post[domRef.options[domRef.selectedIndex].value] = document.getElementById('templateArea').value;
	database.ref('templates/' + userProfile.userKey).update(post);
	templates[domRef.options[domRef.selectedIndex].value] = document.getElementById('templateArea').value;
	
	document.getElementById('message').innerHTML = "Saved.";
	$('#message').fadeIn();
	setTimeout(function() {
		$('#message').fadeOut();
	}, 2000);
}

function manageServiceProviders() {
	var pageContent = '<div class="row col-12" style="text-align: center;"><h1 id="title">Manage Service Providers</h1></div>';
	pageContent += '<div class="row"><div class="col-md-2">&nbsp</div><div class="col-md-8">';
	pageContent += '<table class="table table-hover" id="spTable"><thead><tr><th>Provider</th><th>Contact Name</th><th>Phone</th><th>Email</th><th>Category</th><th>Actions</th></tr></thead><tbody id="tableBody">';
	pageContent += '<tr><td colspan="6" onClick="addProvider()" style="cursor: pointer;"><span class="glyphicon glyphicon-plus"></span> Add new provider....</td></tr>';
	pageContent += '</tbody></table><button type="button" class="btn btn-primary" onClick="window.location.reload()">Save</button></div><div class="col-md-2">&nbsp</div></div>';
	
	document.getElementById('page').innerHTML = pageContent;
	
	database.ref('providers/' + userProfile.userKey).on('child_added', function(snapshot) {
		var tableEntry = '<tr><td>' + snapshot.val()['providerName'] + '</td><td>' + snapshot.val()['providerContact'] + '</td><td>' + snapshot.val()['providerPhone'] + '</td><td>' + snapshot.val()['providerEmail'] + '</td><td>' + snapshot.val()['providerCategory'] + '</td><td style="text-align: right;"><span title="Edit" class="glyphicon glyphicon-pencil" style="cursor: pointer;" onClick="editProvider(this.dataset.target)" data-target="' + snapshot.key + '"></span>&nbsp;<span title="Remove" class="glyphicon glyphicon-remove" style="cursor: pointer;" onClick="confirmRemove(this.dataset.name, this.dataset.target)" data-target="' + snapshot.key + '" data-name="' + snapshot.val()['providerName'] + '"></span></td></tr>';
		document.getElementById('tableBody').innerHTML += tableEntry;
	});
}

function addProvider() {
	document.getElementById('usableModalContent').innerHTML = "";
	var providerFields = ["providerName", "providerContact", "providerPhone", "providerEmail"];
	var fieldLabels = ["Provider", "Contact Name", "Phone", "Email"];
	var fieldDiv = document.createElement('div');
	fieldDiv.innerHTML += '<h1>Add Service Provider</h1>';
	
	providerFields.forEach(function(fieldName, index) {
		var myRep = '<div class="form-group"><label for="' + fieldName + '">' + fieldLabels[index] + '</label><input type="text" class="form-control" id="' + fieldName + '"></div>';
		fieldDiv.innerHTML += myRep;
	});
	
	fieldDiv.innerHTML += '<div class="form-group"><label for="providerCategory">Provider Category</label><br><input type="radio" id="providerCategory" name="providerCategory" value="Photographer">Photographer &nbsp;&nbsp;&nbsp;<input type="radio" id="providerCategory" name="providerCategory" value="Builder">Builder</div>';
	
	document.getElementById('usableModalContent').appendChild(fieldDiv);
	
	document.getElementById('usableModalContent').innerHTML += '<div class="row btn-toolbar"><button type="button" class="btn btn-primary" onClick="saveProvider()">Save</button></div>';
	
	$('#usableModal').modal({backdrop: 'static', keyboard: false});
}

function editProvider(ref) {
	addProvider();
	
	database.ref('providers/' + userProfile.userKey + '/' + ref).once('value').then(function(snapshot) {
		snapshot.forEach(function(childSnapshot) {
			if (childSnapshot.key == 'providerCategory') {
				document.getElementsByName('providerCategory').forEach(function(radio) {
					if (radio.value == childSnapshot.val()) {
						radio.checked = true;
					}
				});
			} else {
				document.getElementById(childSnapshot.key).value = childSnapshot.val();
			}
		});
	});
	
	document.getElementById('usableModalContent').innerHTML += '<div style="display: none" id="providerRef" data-reference="' + ref + '"></div>';
}

function saveProvider() {
	var post = {};
	var providerFields = ["providerName", "providerContact", "providerPhone", "providerEmail"];
	providerFields.forEach(function(fieldName) {
		post[fieldName] = document.getElementById(fieldName).value;
	});
	document.getElementsByName('providerCategory').forEach(function(radio) {
		if (radio.checked) {
			post['providerCategory'] = radio.value;
		}
	});
	var providerRef = document.getElementById('providerRef');
	if (providerRef) {
		database.ref('providers/' + userProfile.userKey + '/' + providerRef.dataset.reference).update(post);
	} else {
		post['ref'] = database.ref('providers/' + userProfile.userKey).push().key;
		database.ref('providers/' + userProfile.userKey + '/' + post.ref).update(post);
	}
	
	document.getElementById('closeUsableModal').click();
	
	// Cheat refresh
	manageServiceProviders();
}

function confirmRemove(providerName, ref) {
	var modal = document.getElementById('usableModalContent');
	modal.innerHTML = '<div><h1>Confirm Delete</h1><p>Are you really sure you want to remove ' + providerName + '?</p></div>';
	modal.innerHTML += '<div class="row btn-toolbar"><button type="button" class="btn btn-primary" onClick="removeProvider(this.dataset.ref)" data-ref="' + ref + '">Yes</button><button type="button" class="btn btn-primary" onClick="document.getElementById(\'closeUsableModal\').click()">No</button></div>';
	
	$('#usableModal').modal({backdrop: 'static', keyboard: false});
}

function removeProvider(ref) {
	
	database.ref('providers/' + userProfile.userKey + '/' + ref).remove().then(function() {
	
		// Cheat refresh
		document.getElementById('closeUsableModal').click();
		manageServiceProviders();
		
	});
}

function displayAfterLogout() {
	goHome();
}

function displayAfterLogin() {
	// Expose the icons
	var domElements = document.getElementsByClassName('dashIconShaded');
	// Changing the class reduces the array size
	while (domElements.length > 0) {	
		
		// Make them clickable
		domElements[0].setAttribute('onClick', 'load(this)');
		
		// Make them prominent
		domElements[0].setAttribute('class', 'dashIcon');
		
	};
	
	// Expose the menu
	document.getElementById('menu').setAttribute('style', 'display: block');
	
	// Hide the login button
	document.getElementById('displayBar').innerHTML = '';
}

// Stolen from stack overflow
function camelize(str) {
  return str.replace(/(?:^\w|[A-Z]|\b\w)/g, function(letter, index) {
    return index == 0 ? letter.toLowerCase() : letter.toUpperCase();
  }).replace(/\s+/g, '');
}