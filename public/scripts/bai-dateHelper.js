// Why is JS date so crap?


function getDayName (day) {
	if (Number.isInteger(day)) {
		var dayList = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
		return dayList[day];
	} else {
		return null;
	}
}

function getMonthFullName (month) {
	if (Number.isInteger(month)) {
		var monthList = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
		return monthList[month];
	} else {
		return null;
	}
}

function getMonthShortName (month) {
	if (Number.isInteger(month)) {
		var monthList = ["Jan", "Feb", "Mar", "Apr", "May", "Jue", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"];
		return monthList[month];
	} else {
		return null;
	}
}

function get12HrTime (hrs, mins) {
	if (Number.isInteger(hrs) && Number.isInteger(mins)) {
		var meridiem = "AM";
		if (hrs > 11) {
			meridiem = "PM";
			hrs -= 12;
		}
		if (hrs == 0) {
			hrs = 12;
		}
		if (mins < 10) {
			return hrs + ":0" + mins + " " + meridiem;
		} else {
			return hrs + ":" + mins + " " + meridiem;
		}
	}
}

function getICSDateTime (argDateTime) {
	var dateTime = new Date(argDateTime);
	var icsDateTime = dateTime.getFullYear();
	icsDateTime += padDigit(dateTime.getMonth() + 1);
	icsDateTime += padDigit(dateTime.getDate()) + 'T';
	icsDateTime += padDigit(dateTime.getHours());
	icsDateTime += padDigit(dateTime.getMinutes());
	icsDateTime += padDigit(dateTime.getSeconds());
	return icsDateTime;
}

function padDigit (digit) {
	if (digit < 10) {
		return '0' + digit;
	} else {
		return digit;
	}
}