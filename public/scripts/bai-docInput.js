
var auth;
var database;
var docRef;
var transaction;
var fieldList;

function docInputInit(docRef, docTitle, activeTransaction) {
	// Quick links to the firebase stuff
    this.database = firebase.database();
	
	// Scope my param
	this.docRef = docRef;
	this.transaction = activeTransaction;
	
	// Draw the page
	pageDiv = '<div class="row col-12" style="text-align: center;"><h1 id="title">' + docTitle + '</h1></div>';
	pageDiv += '<div class="row col-12" style="text-align: right; padding-bottom: 15px;"><p id="message"></p><button type="button" class="btn btn-primary" onClick="activityCentreInit()">Save</button>&nbsp;<button type="button" class="btn btn-primary" onClick="docPrint()">Print</button>&nbsp;<button type="button" class="btn btn-primary" onClick="docEmail()">Email</button></div>';
	pageDiv += '<div class="wrap" style="display: flex"><div class="row" style="overflow: none; height: 100%;">';
	pageDiv += '<div class="col-md-3" id="inputPane" style="overflow: auto; height: 100%;"></div>';
	pageDiv += '<div class="col-md-9" id="displayPane" style="overflow: auto; height: 100%;"></div>';
	pageDiv += '</div></div>';
	document.getElementById('page').innerHTML = pageDiv;
	
	drawDoc();
}

function drawDoc() {
	
	fieldList = [];
	
	var inputDiv = document.getElementById('inputPane');
	var displayDiv = document.getElementById('displayPane');
	$('#message').fadeOut();
	
	database.ref('docs/' + userProfile.userKey + '/' + docRef).once('value', function(snapshot) {
		if (snapshot.val() == null) {
			displayDiv.innerHTML = '<p>Your ' + docTitle + ' template has not been found. Please contact Binding Aitch Industries to arrange conversion and upload of this document.</p>';
		} else {
			displayDiv.innerHTML = decodeURIComponent(snapshot.val().doc);
			snapshot.child('mapping').forEach(function(element) {
				var label = '<label for="' + element.val().field + '">' + element.val().label + '</label>';
				var input;
				if (element.val().type == "text" || element.val().type == "date") {
					input = '<input type="' + element.val().type + '" id="' + element.val().field + '" class="form-control" data-target="'+ element.key +'" onChange="docFill(this)">';
				}
				if (element.val().type == "dropdown") {
					input = '<select id="' + element.val().field + '" onClick="dataToggle(this)">';
					element.child('options').forEach(function(option) {
						input += '<option value="' + option.val().ref + '">' + option.val().label + '</option>';
					});
					input += '</select>';
				}
				inputDiv.innerHTML += '<div class="form-group">' + label + input + '</div>';
				fieldList.push(element.val().field);
			});
			return database.ref('docs/' + userProfile.userKey + '/' + docRef).once('value');
		}
	}).then(function(snapshot) {
		// For repopulation loop over mapping, if transaction has a field element for that map then populate the input text (data-field = field) and span (id = mapping key)
		snapshot.child('mapping').forEach(function(element) {
			if (transaction[element.val().field]) {
				
				if (element.val().type == "text") {
					// Put the value in the form
					document.getElementById(element.val().field).value = transaction[element.val().field];
					
					// Put the value in the document
				    document.getElementById(element.key).innerHTML = transaction[element.val().field];
				}
				if (element.val().type == "date") {
					// Strip out the time portion and only keep the date
					var givenDate = new Date(transaction[element.val().field]).toDateString();
					
					// Put the value in the form
					document.getElementById(element.val().field).value = givenDate;
					
					// Put the value in the document
					document.getElementById(element.key).innerHTML = givenDate;
				}
				if (element.val().type == "dropdown") {
					
					// Put the value in the form
					var domElement = document.getElementById(element.val().field);
					for (index = 0; index < domElement.options.length; index++) {
						if (domElement.options[index].innerHTML == transaction[element.val().field]) {
							domElement.selectedIndex = index;
						}
					}
					
					// Put the value in the document
					dataToggle(domElement);
				}
			}
		});
	});
		
	// Populate any known values
	Object.keys(transaction).forEach(function (key) {
		if (transaction[key] != "") {
			if (document.getElementById(key) != null) {
				document.getElementById(key).value = transaction[key];
			}
		}
	});	
}

function docFill(element) {
	
	if (element.type == "text") {

		// Push data to visible doc
		document.getElementById(element.dataset.target).innerHTML = element.value;
		
		// Put it in my internal record
		transaction[element.id] = element.value;
	} 
	if (element.type == "date") {
		var givenDate = new Date(element.value);
		
		// Push data to visible doc
		document.getElementById(element.dataset.target).innerHTML = givenDate.toLocaleDateString();
		
		// Put it in my internal record
		transaction[element.id] = givenDate.valueOf();
	}
	
	// Check if this entry completes the document
	if (isDocComplete()) {
		var key = docRef + 'Completed';
		transaction[key] = true;
	}
	
	// Push any changes in my record to firebase
	database.ref('transactions/' + userProfile.userKey + '/' + transaction.ref).update(transaction);
	
	
}

function isDocComplete() {
	var complete = true;
	fieldList.forEach(function(field) {
		if (!transaction[field]) {
			complete = false;
		}
	});
	return complete;
}

function dataToggle(element) {
	for (index = 0; index < element.options.length; index++) {
		if (index == element.selectedIndex) {
			document.getElementById(element.options[index].value).setAttribute('style', "display: inline");
			transaction[element.id] = element.options[index].innerHTML;
		} else {
			document.getElementById(element.options[index].value).setAttribute('style', "display: none");
		}
	}
	
	// Push any changes in my record to firebase
	database.ref('transactions/' + userProfile.userKey + '/' + transaction.ref).update(transaction);
}

function docPrint() {
	var printable = window.open();
	printable.document.write('<html><head>' + document.head.innerHTML + '</head><body>' + document.getElementById('displayPane').innerHTML + '</body></html>');
	setTimeout(function() {
		printable.print();
		printable.close();		
	}, 2000);
	
	
	// TODO store a flag to say the pdf has been printed. We can use this as a warning when someone attempts to edit it later.

}

function docEmail() {
	// Send link to doc preview screen....slight obfuscation in regards to ref_1, ref_2, etc so its not plainly obvious that we're sending a user key. Permissions won't let 
	// anything bad happen but it is a bad look we want to avoid.
	
	var modalContent = '<h1>Send Document</h1>';
	modalContent +=  '<div class="form-group"><label for="destEmail">To:</label><input type="text" class="form-control" id="destEmail"></div>';
	modalContent +=  '<div class="form-group"><label for="subject">Subject:</label><input type="text" class="form-control" id="subject"></div>';
	modalContent +=  '<div class="form-group"><label for="emailBody">Email:</label><textarea class="form-control" rows=8 id="emailBody">\n\nTo print this document please follow this link:\nhttps://amicus-510bb.firebaseapp.com/docView.html?ref_1=' + userProfile.userKey + '&ref_2=' + transaction.ref + '&doc=' + docRef +'</textarea></div>';
	modalContent +=  '<div class="row btn-toolbar"><button type="button" class="btn btn-primary" onClick="sendDoc()">Send</button><button type="button" class="btn btn-primary" onClick="document.getElementById(\'closeUsableModal\').click()">Cancel</button></div>'
	
	document.getElementById('usableModalContent').innerHTML = modalContent;
		
		// Put up the modal
    $('#usableModal').modal('show');
}

function sendDoc() {
	// Queue mail
	var mailPost = {};
	mailPost['personalizations'] = [];
	var toArray = [];
	console.log(document.getElementById('destEmail').value);
	toArray.push({email: document.getElementById('destEmail').value});
	mailPost['personalizations'].push({to: toArray, subject: document.getElementById('subject').value});
	mailPost['from'] = {email: userProfile.agentEmail};
	var mailContent = {};
	mailContent['type'] = 'text/plain';
	mailContent['value'] = document.getElementById('emailBody').value;
	mailPost['content'] = [mailContent];

	var jsonEmail = JSON.stringify(mailPost);
	
	database.ref('emailQueue/').push({email: jsonEmail});
	
	// Close the modal
	document.getElementById('closeUsableModal').click();
}

function compareOrder (a, b) {
	if (a.order < b.order)
		return -1;
	if (a.order > b.order)
		return 1;
	return 0;
}